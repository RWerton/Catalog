var catalog = {
    'wear' : [
        {
            'name' : "кофта",
            'minSize' : "xxxs",
            'maxSize' : "xxxl",
            'price' : 999
        },
        {
            'name' : "футболка",
            'minSize' : "xxs",
            'maxSize' : "xxl",
            'price' : 499
        }
    ],
    'shoes' : [
        {
            'name' : "ботинки",
            'minSize' : "38",
            'maxSize' : "45",
            'price' : 1498
        },
        {
            'name' : "кроссовки",
            'minSize' : "35",
            'maxSize' : "42",
            'price' : 2000
        }
    ]
};

document.getElementById("name").innerHTML = "Name:" +" "+ catalog.wear[0].name +" "+ catalog.wear[1]. name;
document.getElementById("minSize").innerHTML = "Min Size:" +" "+ catalog.wear[0].minSize +" "+ catalog.wear[1]. minSize;
document.getElementById("maxSize").innerHTML = "Max Size:" +" "+ catalog.wear[0].maxSize +" "+ catalog.wear[1]. maxSize;
document.getElementById("price").innerHTML = "Price:" +" "+ catalog.wear[0].price +" "+ catalog.wear[1]. price;

document.getElementById("name1").innerHTML = "Name:" +" "+ catalog.shoes[0].name +" "+ catalog.shoes[1]. name;
document.getElementById("minSize1").innerHTML = "Min Size:" +" "+ catalog.shoes[0].minSize +" "+ catalog.shoes[1]. minSize;
document.getElementById("maxSize1").innerHTML = "Max Size:" +" "+ catalog.shoes[0].maxSize +" "+ catalog.shoes[1]. maxSize;
document.getElementById("price1").innerHTML = "Price:" +" "+ catalog.shoes[0].price +" "+ catalog.shoes[1]. price;